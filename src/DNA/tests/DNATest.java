package DNA.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;
import DNA.DR2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class DNATest extends TestCase{

	private void _test(String values, String result) {
		final String input = values;
		
		final String output = "Enter RNA: " + result ;
		
		final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setIn(new ByteArrayInputStream(input.getBytes()));
		System.setOut(new PrintStream(outContent));
		
		DR2.main(new String[] { "foo" });
        {
				assertEquals(output, outContent.toString());
				//System.out.print(out.Content.toString());

		
		System.setIn(null);
		System.setOut(null);
	}
	
  }

private void _testResult(String rna, String gene) {
	_test(rna,gene);
}

public void testAUGUAA() {
_testResult("AUGCCCUAA", "AUGCCCUAA");
}	
public void testAUGUAG() {
_testResult("AUGCCCUAG", "AUGCCCUAG");
}
public void testAUGUGA() {
_testResult("AUGCCCUGA", "AUGCCCUGA");
}
public void testCCCAUGCCCUGACCC() {
_testResult("CCCAUGCCCUGACCC", "AUGCCCUGA");
}
public void testshort() {
_testResult("CCC", "RNA too short");
}
public void teststart() {
_testResult("CCCCCCCCC", "No start codon");
}
public void teststop() {
_testResult("AUGCCCCCC", "Invalid gene codons");
}
}
